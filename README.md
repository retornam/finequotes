# finequotes for fine people

finequotes is a *[Flask](https://flask.palletsprojects.com/en/1.1.x/) 
[Python](https://python.org)* web application that consists of 
a frontend to view inspiring quotes and a backend API to submit
new quotes.


# Install Instructions

- Clone this folder using this link [https://gitlab.com/retornam/finequotes.git](https://gitlab.com/retornam/finequotes.git)

- Install *[docker-compose](https://docs.docker.com/compose/install/)* on your
  operating system of choice

- Change directory to your newly cloned project and run `docker-compose -f docker-compose.yml up -d --build`

- Next run `docker ps` , you should see two containers running with the names
  
  - finequotes_nginx_1
  - finequotes_finequotes_1

- Visit *[http://localhost:8070](http://localhost:8070)* in your browser to view the application.

- To teardown the application run `docker-compose down` in the `finequotes` folder

# Folder Tree

```
├── Dockerfile
├── Pipfile
├── Pipfile.lock
├── README.md
├── conf.d
│   └── flaskapp.conf
├── docker-compose.yml
├── env_file
└── finequotes
    ├── __init__.py
    ├── models
    │   ├── __init__.py
    │   ├── author.py
    │   ├── base.py
    │   └── quote.py
    ├── quotes.db
    ├── requirements.txt
    ├── routes
    │   ├── __init__.py
    │   ├── common.py
    │   ├── quotes.py
    │   └── v1.py
    ├── schema
    │   ├── __init__.py
    │   ├── author.py
    │   ├── quote.py
    │   └── validators
    │       └── __init__.py
    ├── settings.py
    ├── static
    │   ├── boab-tree.png
    │   ├── css
    │   │   └── style.css
    │   ├── favicon.png
    │   ├── img
    │   │   └── wave.png
    │   └── js
    ├── templates
    │   └── quotes
    │       └── index.html
    └── tests
        ├── __init__.py
        ├── fixtures.py
        ├── test-quotes.db
        ├── test_init.py
        └── test_routes.py

13 directories, 33 files
```
