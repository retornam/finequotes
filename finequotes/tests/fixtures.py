import os
import pytest
from finequotes import create_app

ROOT = os.path.abspath(os.path.dirname(__file__))
DBPATH = os.path.join(ROOT, "test-quotes.db")
SECRET_KEY = os.getenv("TEST_SECRET_KEY", "test-secret")
config = {
    "CONFIG_NAME": "test",
    "DEBUG": True,
    "SECRET_KEY": SECRET_KEY,
    "SQLALCHEMY_TRACK_MODIFICATIONS": False,
    "DBPATH": DBPATH,
    "TESTING": True,
    "SQLALCHEMY_DATABASE_URI": f"sqlite:///{DBPATH}",
}


@pytest.fixture
def app():
    return create_app(config)


@pytest.fixture
def client(app):
    return app.test_client()


@pytest.fixture
def db(app):
    from ..models import db

    with app.app_context():
        db.create_all()
        yield db
        db.drop_all()
        db.session.commit()
