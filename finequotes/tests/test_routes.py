from .fixtures import app, client


def test_health_route(app, client):
    with client:
        resp = client.get("/health")
        assert resp.status_code == 200
        assert resp.is_json
        assert resp.json == "Thanks for flying Vim."


def test_api_v1_route(app, client):
    with client:
        resp = client.get("/api/v1/version")
        assert resp.status_code == 200
        assert resp.data == b"v1.0"


# this tests index page when db is empty
def test_index_route(app, client):
    with client:
        resp = client.get("/")
        assert resp.status_code == 200
        assert b"Unknown" in resp.data
