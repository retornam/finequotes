from flask import Blueprint, jsonify
from flask import url_for, redirect, render_template
from ..schema.author import AuthorSchema
from .common import randomquote


bp = Blueprint("quotes", __name__)


@bp.route("/")
def index():
    return render_template("quotes/index.html", randomquote=randomquote())


@bp.route("/health")
def healthy():
    return jsonify("Thanks for flying Vim.")
