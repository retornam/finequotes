import random
from ..models import db
from ..models.author import Author
from ..models.quote import Quote
from ..schema.author import AuthorSchema
from ..schema.quote import QuoteSchema

author_schema = AuthorSchema()
authors_schema = AuthorSchema(many=True)
quote_schema = QuoteSchema()
quotes_schema = QuoteSchema(many=True, only=("id", "content", "author.formatted_name"))


def randomquote():
    try:
        quotes = Quote.query.all()
        result = quotes_schema.dump(quotes, many=True)
        randquote = random.choice(result)
        return {"quotes": randquote}
    except:
        return {
            "quotes": {"author": {"formatted_name": "Unknown"}},
            "content": "Change is good.",
        }
