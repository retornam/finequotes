from . import v1
from . import quotes


def init_app(app):
    app.register_blueprint(v1.bp, url_prefix="/api/v1")
    app.register_blueprint(quotes.bp, url_prefix="")
