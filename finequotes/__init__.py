import os
from flask import Flask
from . import models, routes


def create_app(config=None):
    app = Flask(__name__)
    app.config.from_object("finequotes.settings")
    # FINEQUOTES_CONF is a full path to a file
    if "FINEQUOTES_CONF" in os.environ:
        app.config.from_envvar("FINEQUOTES_CONF")
    if config is not None:
        if isinstance(config, dict):
            app.config.update(config)
        elif config.endswith(".py"):
            app.config.from_pyfile(config)
    with app.app_context():
        models.init_app(app)
        routes.init_app(app)
    return app
