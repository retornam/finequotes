import os


ROOT = os.path.abspath(os.path.dirname(__file__))

DEBUG = False
SQLALCHEMY_TRACK_MODIFICATIONS = False
DBPATH = os.path.join(ROOT, "quotes.db")
SQLALCHEMY_DATABASE_URI = f"sqlite:///{DBPATH}"
