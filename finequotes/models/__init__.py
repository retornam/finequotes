from .base import db
from .author import Author
from .quote import Quote


def init_app(app):
    db.init_app(app)
    db.create_all()
    db.session.commit()
    db.app = app
