from marshmallow import Schema, fields, ValidationError, pre_load
from .validators import must_not_be_blank


class AuthorSchema(Schema):
    id = fields.Int(dump_only=True)
    first = fields.Str()
    last = fields.Str()
    formatted_name = fields.Method("format_name", dump_only=True)

    def format_name(self, author):
        return "{} {}".format(author.first, author.last)


# Custom validator
def must_not_be_blank(data):
    if not data:
        raise ValidationError("Data not provided.")
