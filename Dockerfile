FROM python:3

ENV PYTHONUNBUFFERED 1
ADD finequotes /opt/services/finequotes/
WORKDIR /opt/services/finequotes/
RUN pip install -r /opt/services/finequotes/requirements.txt
EXPOSE 5001

#CMD ["python3", "app.py"]
#CMD ["tail", "-f", "/dev/null"]
#CMD ["flask","run","--host", "0.0.0.0","--port", "5001"]
WORKDIR /opt/services/
CMD ["waitress-serve", "--port=5001", "--call", "finequotes:create_app"]